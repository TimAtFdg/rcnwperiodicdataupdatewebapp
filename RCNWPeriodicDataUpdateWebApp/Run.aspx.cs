﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RCNWPeriodicDataUpdateWebApp
{
    public partial class Run : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RiverChartNWPeriodicDataUpdateUsingVisitFrequency dataUpdate = new RiverChartNWPeriodicDataUpdateUsingVisitFrequency();
            dataUpdate.ExecuteRequest();
            Response.Write("Data update started");
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            return;
        }
    }
}