﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Net;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

using RiverChartDataRequestAndProcessLibrary;

namespace RCNWPeriodicDataUpdateWebApp
{
    public class RiverChartNWPeriodicDataUpdateUsingVisitFrequency
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        delegate void MethodInvoker();

        public void ExecuteRequest()
        {
            MethodInvoker simpleDelegate;

            simpleDelegate = new MethodInvoker(this.RunUpdate);
            simpleDelegate.BeginInvoke(new AsyncCallback(Callback), simpleDelegate);
        }

        private void Callback(IAsyncResult ar)
        {
            MethodInvoker simpleDelegate = (MethodInvoker)ar.AsyncState;
            Debug.WriteLine("Calling EndInvoke");
            simpleDelegate.EndInvoke(ar);
        }

        private void RunUpdate()
        {
            try
            {
                int numberToTake = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["MaxUpdateNumber"]);
                using (var context = new RiverChartDataRequestAndProcessLibrary.RiverChartEntities())
                {
                    ChartDataRepository chartDataRepository = new ChartDataRepository();
                    log.Info(String.Format("Starting time: {0}\n", DateTime.Now.ToShortTimeString()));
                    var stationItems = context.DataItems.Where(q => q.StationInformation.UTCTimeOffset != 0 && (q.lastCount > 0 || q.currentCount > 0)).OrderByDescending(q => q.lastCount).ThenByDescending(q => q.currentCount).Take(numberToTake);
                    // get list of current files
                    var files = new DirectoryInfo(System.Web.Configuration.WebConfigurationManager.AppSettings["DataDirectory"].ToString()).GetFiles();
                    foreach (var file in files)
                    {
                        string[] filePieces = file.Name.Split(new char[] { '_' });
                        string stationId = filePieces[0];
                        string peType = filePieces[1];
                        if (filePieces.Length == 2)
                        {
                            if (DateTime.UtcNow - file.CreationTimeUtc > TimeSpan.FromHours(1.0) && stationItems.Where(q => q.stationID == stationId && q.PEType == peType).Count() == 0)
                            {
                                // file does not exist in top list so delete because it is too old
                                bool fileDeleted = false;
                                int maxAttempts = 10;
                                while (!fileDeleted && maxAttempts > 0)
                                {
                                    try
                                    {
                                        File.Delete(file.FullName);
                                        log.Info("Deleting file " + file.Name);
                                        fileDeleted = true;
                                    }
                                    catch
                                    {
                                        maxAttempts--;
                                        Thread.Sleep(1000);
                                    }
                                }
                            }
                        }
                        else
                            File.Delete(file.FullName);
                    }


                    foreach (DataItem item in stationItems)
                    {
                        log.Info(String.Format("Getting data for {0}_{1}\n", item.stationID, item.PEType));
                        ChartData chartData = chartDataRepository.Get(item);
                        if (chartData.DataRetrievedSuccessfully)
                        {
                            string jsonData = JsonConvert.SerializeObject(chartData);
                            string filePath = System.Web.Configuration.WebConfigurationManager.AppSettings["DataDirectory"].ToString() + item.stationID + "_" + item.PEType;
                            using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                            {
                                using (StreamWriter sw = new StreamWriter(fs))
                                {
                                    sw.Write(jsonData);
                                }
                            }
                            item.statusMessage = "ok";
                            item.statusMessageDateTime = DateTime.UtcNow;
                        }
                        else
                        {
                            item.statusMessage = chartData.ErrorMessage;
                            item.statusMessageDateTime = DateTime.UtcNow;
                        }
                    }
                    context.SaveChanges();
                }
                log.Info(String.Format("Ending time: {0}\n", DateTime.Now.ToShortTimeString()));
            }
            catch (Exception e)
            {
                log.Error("Exception: " + e.Message);
            }
        }
    }
}